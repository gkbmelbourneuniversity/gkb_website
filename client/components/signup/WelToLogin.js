/*
 * This component renders a welcome section on login page
 * It is used by homepage and map login
 */

import React from 'react';
import { Link } from 'react-router';
import logo from '../img/uom_logo.gif';

class WelToLogin extends React.Component {
    render() {
        return (
	    <div className="row">
            <div className="col-12 welcome-container">
		<div className="row">
                <h1 className="col-12 welcome-title">Welcome</h1>
                <div className="col-12">
                    <img src={logo}/>
                </div>
                <div className="col-12 welcome-text">
                    <p>Already have an account?</p>
                </div>
                <Link to="/login" className="col-12 btn btn-default btn-welcome">Login</Link>
                </div>
            </div>
            </div>
        );
    }
}

export default WelToLogin;
