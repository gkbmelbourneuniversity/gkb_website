/**
 * @file Component for show nearby landmark
 * Created by TinaAcc on 8/7/17.
 */

import React from 'react';
import config from '../../../server/config.js';

class Nearby extends React.Component {

    constructor(props) {
        super(props);
    }

    direction(key, e){
        this.props.requestDirection({lat: parseFloat(e.lattitude), lng: parseFloat(e.longitude)})
    }

    render() {
	const landmarks = this.props.landmarks
	var view

	if (landmarks && landmarks != "NOT FOUND") {
            var list = []
            for (var key in landmarks){
                list.push(
                   <li key={key}>
                       <a onClick={this.direction.bind(this, key, landmarks[key])}
                          href="#">{key}
                       </a>
                   </li>
                )
	    }
	    view = list
        } else {
	    view = <p>No landmark is found</p>
	}

        return (
	    <div className="row">
                 <div className="nearby_tag col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <h3><b>Nearby</b></h3>
                     <div className="landmark-sec" id='LandmarkDirectionResult'>
                         {view}
                     </div>
                 </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        login: state.login,
        searchResult: state.searchResult
    };
}
export default Nearby;
