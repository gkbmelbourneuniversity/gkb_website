/*
 * This component provide a link for users quickly turn to Signup Page
 */

import React from 'react';
import { Link } from 'react-router';
import logo from '../img/uom_logo.gif';

class WelToSignup extends React.Component {
    render() {
        return (
	<div className="row">
            <div className="welcome-container">
		<div className="row">
                <h1 className="welcome-title col-12">Welcome</h1>
                <div className="col-12">
                    <img src={logo} className="logo"/>
                </div>
                <div className="col-12 welcome-text">
                    <p>Sign up here</p>
                </div>
                <Link to="/signup" className="col-12 btn btn-default btn-welcome">Signup</Link>
                </div>
            </div>
        </div>
        );
    }
}

export default WelToSignup;
