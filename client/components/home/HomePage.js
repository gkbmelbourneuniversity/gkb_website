/**
 * @file Home page with search result
 */

import React from 'react';
import { connect } from 'react-redux';
import Profile from '../userProfile/Profile';
import NavBar from './NavBar';
import { logout } from '../../actions/authAction';
import DirectionPage from '../navigation/DirectionPage'
import SearchPage from './SearchPage';
import { setShowSearchResult} from '../../actions/setShowSearchResult'

class HomePage extends React.Component {

    constructor(props){
        super(props);

	this.state = {
            showResultPage: true,
            showAutoSuggest: false,
	}

	this.closePage = this.closePage.bind(this)
	this.setAutoSuggest = this.setAutoSuggest.bind(this)
	this.hideResultPage = this.hideResultPage.bind(this)
    }

    // Hide result page but search bar
    hideResultPage() {
	this.setState({ showResultPage: false })
    }

    // Close all page but search bar
    closePage() {
        this.props.setShowSearchResult({showSearchResult: false,})
        this.setState({
            showResultPage: false,
            showAutoSuggest: false,
        })
    }

    // Toggle auto suggestions
    setAutoSuggest(show) {
        this.setState({ showAutoSuggest: show })
    }

    render() {
        const { isAuthenticated } = this.props.login;

	var placeHolder = "City, State, Country"
	if (this.props.route.showDirection ||
            (this.state.showResultPage && this.props.searchResult["searchResultPageConfig"])) {
            placeHolder = this.props.searchResult["searchResultPageConfig"]["placeFullAddr"]
	}

        return (
            <div className="container-fluid float_on_the_map_outer">
                <NavBar login = {this.props.login} logout={ this.props.logout} />
                {isAuthenticated && <Profile login = {this.props.login} />}

		<div className="row">
		    <div className="col-lg-3 col-md-5 col-sm-8 col-xs-8 home-container">
		        {this.props.route.showDirection ?
		        <DirectionPage
		            id= {this.props.login.user.osm_id}
		            type = {this.props.login.user.osm_type}
		            requestDirection = {this.props.route.requestDirection}
		            directions = {this.props.route.directions}
			    landmarks = {this.props.route.landmarks}
                            hideSearchResult={this.hideSearchResult}
                            placeholder = {placeHolder}
                            setSearchLocation = {this.props.route.setSearchLocation}
			    closePage = {this.closePage}
		            setAutoSuggest = {this.setAutoSuggest}
		            hideResultPage = {this.hideResultPage}
                            setShowDirection={this.props.route.setShowDirection}
		        />
		        :
			<SearchPage searchResult={this.props.searchResult}
			    showResultPage = {this.state.showResultPage}
                            isAuthenticated = {isAuthenticated}
                            login={this.props.login}
                            user_id={this.props.login.user._id}
                            requestDirection={this.props.route.requestDirection}
                            setShowDirection={this.props.route.setShowDirection}
			    showAutoSuggest = {this.state.showAutoSuggest}
                            hideSearchResult={this.hideSearchResult}
                            placeholder = {placeHolder}
                            storeSearchString = {this.storeSearchString}
                            setSearchLocation = {this.props.route.setSearchLocation}
			    closePage = {this.closePage}
	                    setAutoSuggest = {this.setAutoSuggest}
	                    hideResultPage = {this.hideResultPage}
                        />
		        }
                    </div>
                </div>
            </div>
        )
    }
}

HomePage.propTypes = {
    login: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        login: state.login,
        searchResult: state.searchResult,
    };
}

export default connect(mapStateToProps, {
   logout,
   setShowSearchResult,
})(HomePage);
