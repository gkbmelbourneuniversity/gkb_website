/*
 * This component renders the content of search history page
 * This is used by search history component
 */

import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import { browserHistory } from 'react-router';
import { Link } from 'react-router'

import { searchBarRequest } from '../../actions/searchBarAction';
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setShowSearchResult } from '../../actions/setShowSearchResult';
import { setDescriptionArray }from '../../actions/setDescriptionArray';
import { googlePlaceSearchRequest } from '../../actions/googlePlaceSearch.js';

class SearchHistoryList extends React.Component{
    constructor(props){
        super(props);
        this.linkClick = this.linkClick.bind(this);
    }

    linkClick(searchStr) {
        const {user} = this.props.login;
        var toSend = {
            searchStr: "",
            user_id: user._id,
            fulladdr: searchStr,
            button: false
        }

        this.props.searchBarRequest(toSend)
            .then(
                (res) => {
                    const userData = res.data.token;
                    this.props.updateCoordsRequest(userData);
                    const conf = {
                        showSearchResult: true,
                        placeFullAddr:userData.placeFullAddr,
                        placePhoto: "",
                        type: "jena"
                    }
                    if (userData.descriptionArray) {
                        this.props.setDescriptionArray(userData.descriptionArray);
                    } else {
                        this.props.setDescriptionArray({});
                    }
                    this.props.setShowSearchResult(conf);
                    browserHistory.push('/home');
                },
                (err) => {
                    var photo = "";
                    var obj = {
                      searchStr: searchStr
                    }
                    this.props.googlePlaceSearchRequest(obj).then(
                        (res) => {
                            const data = res.data.obj;
                            user.coords = {lat: data.lat, longt: data.lng};
                            console.log("err  response: ", err.response)
                            if (err.response.data.searchHistory) {
                                console.log("err.response.searchHistory: ", err.response.data.searchHistory)
                                user.searchHistory = err.response.data.searchHistory;
                            }

                            if (err.response.data.autoDescription) {
                                console.log("err.response.autoDescription searchHistory: ", err.response.data.autoDescription)
                                user.autoDescription = err.response.data.autoDescription;
                            } else {
                                user.autoDescription = null
                            }

                            // Changing thw whole functionality. For now let descriptionArray is null
                            var descriptionArray = err.response.data.descriptionArray;
                            const conf = {
                                showSearchResult: true,
                                placeFullAddr:searchStr,
                                placePhoto: data.photo,
                                type: "google"
                            }
                            this.props.setShowSearchResult(conf);
                            if (descriptionArray) {
                                this.props.setDescriptionArray(descriptionArray);
                            } else {
                                this.props.setDescriptionArray({});
                            }
                            this.props.updateCoordsRequest(user);
                            browserHistory.push('/home');
                        },
                        (err) => {
                            console.log("in googlePlaceSearch error client");
                        }
                    )
                }
            );
    }

    render(){
        var dates = [];
        var items = [];

        var searchHistory = this.props.searchHistory;
        searchHistory.sort(function(a,b) {
            return  new Date(b.date).getTime() - new Date(a.date).getTime()
        });
        searchHistory.forEach(function(history){
            history.formatedDate = moment(history.date).format('MMMM Do YYYY');
        });

        var currentDate = null;
        var j = 100;
        for(var i = 0; i< searchHistory.length;i++){
            var searchString = searchHistory[i].searchStr
            if(searchHistory[i].formatedDate ===currentDate) {
                items.push(
                    <div key={i} className="entry-block">
                        <div className="search-entry-content col-md-11">
                            <Link onClick={this.linkClick.bind(this, searchString)}>{searchHistory[i].searchStr}
                                <input className="checkbox-on-entry" name="cb" id="cb" type="checkbox"/>
                            </Link>
                        </div>
                    </div>
                )
            } else {
                currentDate = searchHistory[i].formatedDate;
                items.push(
                    <div key={i} className="entry-block">
                        <div className="search-entry-date col-md-4">
                            <p>{searchHistory[i].formatedDate}
                            </p>
                        </div>
                        <div className="search-entry-content col-md-11">
                            <Link onClick={this.linkClick.bind(this, searchString)}>{searchHistory[i].searchStr}
                                <input className="checkbox-on-entry" name="cb" id="cb" type="checkbox"/>
                            </Link>
                        </div>
                    </div>
                )
            }
        }
        return(
            <div className="search-history-block">
                {items}
            </div>
        );
    }
}


SearchHistoryList.propTypes = {
    searchHistory: React.PropTypes.array.isRequired,
    searchBarRequest: React.PropTypes.func.isRequired,
    login: React.PropTypes.object.isRequired,
    updateCoordsRequest: React.PropTypes.func.isRequired,
    setDescriptionArray: React.PropTypes.func.isRequired,
    setShowSearchResult: React.PropTypes.func.isRequired,
    googlePlaceSearchRequest: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, {
    searchBarRequest,
    updateCoordsRequest,
    setShowSearchResult,
    setDescriptionArray,
    setShowSearchResult,
    googlePlaceSearchRequest
})(SearchHistoryList);
