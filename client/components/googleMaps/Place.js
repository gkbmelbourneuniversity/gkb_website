/**
 * @file Place server to analyse shared URL
 */

import React, {Component} from "react";
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { searchBarRequest } from '../../actions/searchBarAction';
import { setShowSearchResult} from '../../actions/setShowSearchResult'
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setDescriptionArray }from '../../actions/setDescriptionArray';

class Place extends Component {
    constructor(props){
        super(props);

        this.getRequestCtx = this.getRequestCtx.bind(this);
        this.handlePlace = this.handlePlace.bind(this);
    }

    componentWillMount() {
        const component = this
        let service = new google.maps.places.PlacesService(document.createElement('div'));
        service.getDetails({
            placeId: this.props.params.place_id,
            }, function(place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    component.handlePlace(place);
                }
        })
    }

    getRequestCtx(searchStr, user, fulladdr, isBtn, description=null, coordinate=null) {
        return {
            searchStr: searchStr,
            user_id: user,
            fulladdr: fulladdr,
            button: isBtn,
            suggest: description,
            coord: coordinate
        }
    }

    handlePlace(place) {
        var photo = ""
        if (place.photos) {
            photo = place.photos[0].getUrl({'maxWidth': 402, 'maxHeight': 268})
        }
        var coordinate = {
            latitude: place.geometry.location.lat(),
            longitude: place.geometry.location.lng(),
            title: place.name,
        }

        var toSend = this.getRequestCtx(place.name, null, place.formatted_address, false, place.formatted_address, coordinate)
        this.props.searchBarRequest(toSend)
            .then((res) => {
                const userData = res.data.token

                this.props.updateCoordsRequest(userData);
                const conf = {
                    showSearchResult: true,
		    place_id: place.place_id,
                    placeFullAddr:userData.placeFullAddr,
                    placePhoto: photo,
                    type: "jena"
                }
                if (userData.descriptionArray) {
                    this.props.setDescriptionArray(userData.descriptionArray);
                } else {
                    this.props.setDescriptionArray({});
                }
                this.props.setShowSearchResult(conf);
            }, (err) => {
		const conf = {
		    showSearchResult: true,
		    place_id: place.place_id,
		    placeFullAddr: place.formatted_address,
		    placePhoto: photo,
		    type: "google"
		}
		this.props.setShowSearchResult(conf);
            }
        );
        browserHistory.push('/home');
	this.props.route.setSearchLocation(coordinate.latitude, coordinate.longitude)
    }

    render() {
        return(<div></div>)
    }
}

function mapStateToProps(state) {
    return {
    };
}

export default connect(mapStateToProps, {
    searchBarRequest,
    updateCoordsRequest,
    setDescriptionArray,
    setShowSearchResult,
 })(Place);
