/*
 * This component is the search bar, it provides auto suggestions, also search button function.
 */

import React, {Component} from "react";
import GooglePlacesSuggest from "react-google-places-suggest";
import "react-google-places-suggest/lib/index.css";
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { addSearchHistory } from '../../actions/addSearchHistory';
import '../../../public/css/nprogress.css';
import NProgress from '../../actions/nprogress';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { searchBarRequest } from '../../actions/searchBarAction';
import { setShowSearchResult} from '../../actions/setShowSearchResult'
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setDescriptionArray }from '../../actions/setDescriptionArray';
import { setGoButtonResultsArray } from '../../actions/goButtonResults'

class MyGoogleSuggest extends Component {
    constructor(props){
        super(props);
        this.state = {
            searchStr: '',
            blocking: false,
        }
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleSelectSuggest = this.handleSelectSuggest.bind(this);
        this.searchBtnClick = this.searchBtnClick.bind(this);
        this.toggleBlocking = this.toggleBlocking.bind(this);
        this.getRequestCtx = this.getRequestCtx.bind(this);
    }

    toggleBlocking() {
        this.setState({blocking: !this.state.blocking});
    }

    getRequestCtx(searchStr, user, fulladdr, isBtn, description=null, coordinate=null) {
        return {
            searchStr: searchStr,
            user_id: user,
            fulladdr: fulladdr,
            button: isBtn,
            suggest: description,
            coord: coordinate
        }
    }

    searchBtnClick(e) {
        const {user} = this.props.login;
        NProgress.start();
        this.setState({ blocking: true });

        var toSend = this.getRequestCtx(this.state.searchStr, user._id, "", true);
        this.props.searchBarRequest(toSend)
            .then((res) => {
                console.log("success in clicking button, back to clientside");
                if(this.props.landingPageFlag != true) {
                    this.props.setGoButtonResultsArray(res.data.results);
                    this.props.setAutoSuggest(true);
                    this.props.hideResultPage();
                    NProgress.done();
                    this.setState({
                        blocking: false,
                        searchStr: "",
                    })
                } else {
                    this.props.setGoButtonResultsArray(res.data.results)
                    NProgress.done();
                    this.setState({
                        blocking: false,
                        searchStr: "",
                    });
                    browserHistory.push('/home');
                }},
            (err) => {
                console.log("err in clicking button, back to clientside");
                this.setState({blocking: false });
            }
        );
    }

    handleSearchChange(e) {
        this.setState({searchStr: e.target.value})
    }

    handleSelectSuggest(suggest, coordinate, place) {
        // query apache jena database, if not then go with google.
        const {user} = this.props.login;
        var place_id = place.place_id
        var photo = ""
        if (place.photos) {
            photo = place.photos[0].getUrl({'maxWidth': 402, 'maxHeight': 268})
        }

        this.setState({blocking: true });
        NProgress.start();

        var toSend = this.getRequestCtx(suggest.terms[0].value, user._id, place.formatted_address, false, suggest.description, coordinate)
        this.props.searchBarRequest(toSend)
            .then((res) => {
                const userData = res.data.token
                NProgress.done();
                this.setState({blocking: false });

                this.props.updateCoordsRequest(userData);
                const conf = {
                    place_id: place_id,
                    showSearchResult: true,
                    placeFullAddr:userData.placeFullAddr,
                    placePhoto: photo,
                    type: "jena"
                }
                if (userData.descriptionArray) {
                    this.props.setDescriptionArray(userData.descriptionArray);
                } else {
                    this.props.setDescriptionArray({});
                }
                this.props.setShowSearchResult(conf);
            }, (err) => {
               NProgress.done();
               this.setState({
                   blocking: false,
                   searchStr: suggest.description,
	       })
               user.coords = { lat: coordinate.latitude, longt: coordinate.longitude}
	       //reset preivous search result
	       var country = ""
	       if (place.address_components && place.address_components.length > 0) {
                  place.address_components.forEach((item) => {
                      if (item.types[0] == "country") {
	                  country = item.long_name
		      }
	          })
	       }
	       user.country = country
	       user.osm_id = ""
	       user.osm_type = ""
	       user.placeFullAddr = ""

               if (err.response.data.searchHistory) {
                   console.log("err.response.searchHistory: ", err.response.data.searchHistory)
                   user.searchHistory = err.response.data.searchHistory;
               }
               if (err.response.data.autoDescription) {
                   console.log("err.response.autoDescription: ", err.response.data.autoDescription)
               }
               user.autoDescription = err.response.data.autoDescription;

               const conf = {
                   showSearchResult: true,
                   place_id: place_id,
                   placeFullAddr:suggest.description,
                   placePhoto: photo,
                   type: "google"
               }
               this.props.setShowSearchResult(conf);

               var descriptionArray = err.response.data.descriptionArray;
               if (descriptionArray) {
                   this.props.setDescriptionArray(descriptionArray);
               } else {
                   this.props.setDescriptionArray({});
               }
               this.props.updateCoordsRequest(user);
            }
        );
        if (this.props.landingPageFlag == true) {
            browserHistory.push('/home');
        }
	this.props.setSearchLocation(coordinate.latitude, coordinate.longitude)
    }

    render() {
        const {searchStr} = this.state;

        return (
	<div className="row">
	    <div className="col-lg-12">
            <BlockUi tag="div" blocking={this.state.blocking}>

            <GooglePlacesSuggest
                googleMaps={google.maps}
                onSelectSuggest={this.handleSelectSuggest}
                search={searchStr}
            >
                <script src='nprogress.js'></script>
                <link rel='stylesheet' href='nprogress.css'/>

                <div className="no-click-through input-group search-bar-box">
                    <input
                        type="text"
                        value={searchStr}
                        className="form-control"
                        placeholder={this.props.placeholder}
                        onChange={this.handleSearchChange}
		        style={{border: 0}}
                    />
                    <span className="input-group-addon">
                        <button className="btn-default glyphicon glyphicon-search"
		                style={{border: 0}}
                                onClick={this.searchBtnClick}
                        />
                    </span>
                    { !this.props.hideCross &&
                    <span className="input-group-btn">
                        <button className="btn btn-default glyphicon glyphicon-remove" type="button"
		                style={{border: 0}}
                                onClick={this.props.closePage} />
                    </span>
                    }
                </div>
            </GooglePlacesSuggest>
            </BlockUi>
	    </div>
	</div>
        )
    }
}

MyGoogleSuggest.propTypes = {
    login: React.PropTypes.object.isRequired,
    landingPageFlag: React.PropTypes.any
}

MyGoogleSuggest.contextTypes = {
    router: React.PropTypes.object.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
        login: state.login
    };
}


export default connect(mapStateToProps, {
    addSearchHistory,
    setShowSearchResult,
    searchBarRequest,
    updateCoordsRequest,
    setDescriptionArray,
    setGoButtonResultsArray,
 })(MyGoogleSuggest);
