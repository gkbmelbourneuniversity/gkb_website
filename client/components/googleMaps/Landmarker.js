/**
 * @file Marker for landmark on map
 */

import React, {Component} from "react"
import { Marker, InfoWindow } from "react-google-maps";

class Landmarker extends Component {

    constructor(props) {
      super(props);

      this.state = { showInfo: false }
      this.toggleLandmarks = this.toggleLandmarks.bind(this);
    }

    toggleLandmarks() {
        this.setState({ showInfo : !this.state.showInfo})
    }

    render() {
      return (
          <div>
          <Marker key={this.props.key}
                  position={ this.props.position }
                  onClick={this.toggleLandmarks}
          >
                  {this.state.showInfo && <InfoWindow>
                  <p>{this.props.name}</p>
                  </InfoWindow>}
          </Marker>
          </div>
      )
    }
}

Landmarker.propTypes = {
    position: React.PropTypes.object.isRequired,
    name: React.PropTypes.object.isRequired,
    key: React.PropTypes.object.isRequired,
}

export default Landmarker;
