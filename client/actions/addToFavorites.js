/**
 * @file
 * Action to interact between client and server
 */

import { SET_FAVORITES } from '../actions/types';
import axios from 'axios';
import {SET_CURRENT_USER_LOGIN} from '../actions/types';

/**
 * Set favourite data in temp storage
 * @param favoriteData Data to be set
 */
export function setFavoriteAction(favoriteData) {
    return {
        type: SET_FAVORITES,
        data: favoriteData
    }
}

/**
 * Set user data in temp storage
 * @param user User to be set
 */
export function setCurrentUser(user){
    return {
        type:SET_CURRENT_USER_LOGIN,
        user
    }
}

/**
 * Add favourite to data base
 * @param user Current user data in temp
 * @param favourite Favourite to be set in database
 */
export function addToFavoritesAction(user, favorite) {
    return dispatch => {
        return axios.post('/api/favorites', favorite).then(res =>{
            var favoriteData = {
                addr: res.data.token.addr,
                image: res.data.token.image,
                coords: res.data.token.coords
            }

            user.favorites = res.data.token.user.favorites
            dispatch(setFavoriteAction(favoriteData));
            dispatch(setCurrentUser(user));
        });
    }
}

/**
 * Remove favourite from database
 * @param favourite Favourite to be removed from database.
 */
export function removeFavoritesAction(favorite) {
    return dispatch => {
        return axios.post('/api/removeFavorites', favorite).then(res => {
            var user= res.data.token.user;
            dispatch(setCurrentUser(user));
        })
    }
}
