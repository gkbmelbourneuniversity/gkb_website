/*
 * This component is the search bar, it provides auto suggestions, also search button function.
 */

import React, {Component} from "react";
import GooglePlacesSuggest from "react-google-places-suggest";
import { connect } from 'react-redux';
import GoogleAutoSuggest from '../googleMaps/GoogleAutoSuggest';

class DirectionSuggest extends Component {
    constructor(props){
        super(props);
        this.state = {
	    searchStr: "",
	    value: "",
        }

        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleSelectSuggest = this.handleSelectSuggest.bind(this);
        this.buttonClick = this.buttonClick.bind(this)
	this.closePage = this.closePage.bind(this)
    }

    buttonClick(e) {
        e.key = "Enter";
        this.desInput.handleKeyDown(e)
    }

    handleSearchChange(e) {
        this.setState({searchStr: e.target.value})
    }

    handleSelectSuggest(suggest, coordinate, place, directionsResponse) {
	this.props.requestDirection(
	    { lat:parseFloat(coordinate.latitude), lng:parseFloat(coordinate.longitude) }
	);
    }

    closePage() {
        this.props.setShowDirection(false)
    }

    render() {
        const {searchStr} = this.state;

	var originAddr = this.props.placeholder
	var destAddr = "Direct to..."
	var steps
	if (this.props.directions && this.props.directions != "") {
	   steps = this.props.directions["routes"][0]["legs"][0]["steps"]

	   originAddr = this.props.directions["routes"][0]["legs"][0]["start_address"]
	   destAddr = this.props.directions["routes"][0]["legs"][0]["end_address"]
	}

	var landmarks = []
	if (this.props.landmarks) {
	    landmarks = this.props.landmarks
	}

        return (
	<div className="row">
	    <div className="col-lg-12 no-click-through direction-container">
                <div className="row">
                    <div className="col-lg-12 page-up">
		    <button className="btn close glyphicon glyphicon-remove" type="button"
		        onClick={this.closePage} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 page-up">
                    <GoogleAutoSuggest
                         landingPageFlag = {false}
                         hideSearchResult={this.props.hideSearchResult}
                         placeholder = {originAddr}
                         setSearchLocation = {this.props.setSearchLocation}
                         setAutoSuggest = {this.props.setAutoSuggest}
                         hideResultPage = {this.props.hideResultPage}
                         hideCross = {true}
                    />
                    </div>
                </div>

		<div className="row">
		    <div className="col-lg-12 page-bottom">
                        <GooglePlacesSuggest
                            googleMaps = {google.maps}
                            onSelectSuggest={this.handleSelectSuggest}
                            search={searchStr}
                            ref={(r)=> this.desInput = r}
                        >
		            <div className="input-group search-bar-box">
                            <input
                                type="text"
                                value={searchStr}
                                className="form-control"
                                onChange={this.handleSearchChange}
		                placeholder={destAddr}
                                style={{border: 0}}
                            />
		            <span className="input-group-addon">
		                <button className="btn-default glyphicon glyphicon-search"
                                     style={{border: 0}}
                                     onClick={this.buttonClick}
		                />
		            </span>
	                    </div>
                        </GooglePlacesSuggest>
	            </div>
	        </div>
		<div className="row">
		     <div className="col-lg-12 direction-page-desc pre-scrollable scrollbar-primary">
		         {steps && steps.map(function(value, key) {
			     if (value) {
			         var inner = "&#8226;" + value["instructions"]
			         return(
			             <div key={key} className="direction-row">
	                             <div dangerouslySetInnerHTML={{__html: inner}} />
			             <div>{value['duration']['text']}</div>

				     {landmarks &&
				      landmarks[key+1] &&
				      landmarks[key+1]["name"] &&
			                 <div className="direction-landmark">Landmarks:
                                              <b>{landmarks[key+1]["name"]}</b>
                                         </div>
				     }
				     </div>
				)
			     }
			 })}
		     </div>
	        </div>
	    </div>
	</div>
        )
    }
}

DirectionSuggest.contextTypes = {
    router: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
    };
}

export default connect(mapStateToProps, null)(DirectionSuggest);
