/*
 * Landing page is the first view when a user visit this applicaiton
 */

import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import NavBar from '../home/NavBar';
import {connect} from 'react-redux';
import GoogleAutoSuggest from '../googleMaps/GoogleAutoSuggest'
import MapContainer from '../googleMaps/MapContainer'


class landingPage extends React.Component{
    render(){
        return(
            <div className="jumbotron">
                <div className="col-md-12 landingPage">
                    <div>
                        <NavBar login={this.props.login}/>
                    </div>
		    <div className="container">
                        <br/>
                        <h3 className="col-12 title-white">
		            Locate your destination in one sentence
		        </h3>
                        <br/>
                        <div className="col-12 landing-search-bar" >
                        <GoogleAutoSuggest
                            landingPageFlag = {true}
                            hideCross = {true}
                            showAutoSuggest={this.showAutoSuggest}
                            hideSearchResult={this.hideSearchResult}
                            placeholder = {"City, State, Country"}
                            setSearchLocation = {this.props.route.setSearchLocation}>

                            <Router history={browserHistory}>
                                <Route path="/" component={MapContainer}/>
                                <Route path="home" component={MapContainer}/>
                                <Route path="map" component={MapContainer}/>
                            </Router>
                        </GoogleAutoSuggest>
                        </div>
                        <br/>
                        <a className="btn btn-info col-12"
		           href="#" role="button">
		           Learn more
		        </a>
                    </div>
                </div>
            </div>
        );
    }
}

landingPage.propTypes = {
    login: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        login: state.login,
    };
}

export default connect(mapStateToProps,{})(landingPage);
