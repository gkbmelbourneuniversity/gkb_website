/*
 * This route handles favorite requests
 */

import express from 'express';
var User = require('./../models/user.js');
var GooglePlaces = require('./../models/googlePlaces')

let router = express.Router();
router.post('/', (req, res) => {
    console.log("in addFavorites route");
    var placePhoto = req.body.photo
    var addr = req.body.location
    var amenity = req.body.amenity
    var country = req.body.country
    var coords = req.body.coords
    var id = req.body.user_id
    var autoComment = req.body.autoComment
    var type = req.body.type == "google" ? "google" : "jena"
    var place_id = req.body.place_id

    console.log("placePhoto: ", placePhoto)
    console.log("addr: ", addr)
    console.log("coords: ", coords)
    console.log("id: ", id)

    var query = {
      addr: addr,
    }
    var place = {
      addr: addr,
      image: placePhoto,
      coords: coords,
    }

    GooglePlaces.find(query).count(function(err, count){
        if(count === 0){
            GooglePlaces.create(place, function(err, dataGoogle) {
                console.log("Writing to db");
                if(err){
                    console.log(err.statusCode);
                }else if(!dataGoogle){
                    console.log(res.statusCode);
                    console.log("Error saving");
                }else{
                    console.log(res.statusCode);
                    console.log("Registered");
                }
            })
        }
    })

    //Find user
    User.findOne({_id: id}, function(err, user) {
        if (err || !user) {
            console.log(err);
        } else {
            //Find favorite
            User.findOne({_id:id,'favorites.searchStr': addr}, function(err, data) {
                if(err) {
                    console.log(err);
                } else if (!data){
                    var insertToFavoritesNew = {
                        type: type,
                        searchStr: addr,
                        amenity: amenity,
                        country: country,
			place_id: place_id,
                        image: placePhoto,
                        coords: coords,
                        date: new Date()
                    }

                    User.update(
                        { _id: id},
                        {$addToSet: { favorites: insertToFavoritesNew}},
                        function(err, updatedUser) {
                            if (err) {
                                console.log(err)
                            } else {
                                var favorites = user.favorites;
                                favorites.push(insertToFavoritesNew);
                                user.favorites = favorites;

                                var token = {
                                    addr: addr,
                                    image: placePhoto,
                                    coords: coords,
                                    user: user._doc
                                }
                                res.status(200).json({token});
                            }
                        }
                    )
                } else {
                    //Favorite exists, update date
                    User.update(
                        { 'favorites.searchSt': addr},
                        {$set: { 'favorites.$.date': new Date()}},
                        function(err, updated) {
                            if (err) {
                                console.log(err);
                            } else {
                                var token = {
                                    addr: addr,
                                    image: placePhoto,
                                    coords: coords,
                                    user: user._doc
                                }
                                res.status(200).json({token});
                            }
                        }
                    )
                }
            })
        }
    })
})

export default router;
