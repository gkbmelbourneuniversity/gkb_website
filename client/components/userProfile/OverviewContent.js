/*
 * This component renders the user overview page
 * This is used by account setting
 */

import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

class OverviewContent extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        const { user } = this.props.login;
        const date = moment(user.date).format('MMMM Do YYYY')

        return(
            <div className="middle-block">
                <h4 className="setting-content-title">Overview/Profile</h4>
                <div className="setting-content-photo">
                </div>
               <table className="table">
                    <tr>
                        <th>Display Name</th>
                        <td>{user.userName}</td>
                    </tr>
                   <tr>
                       <th>Date joined</th>
                       <td>{date}</td>
                   </tr>
                   <tr>
                       <th>Contributions</th>
                       <td>0</td>
                   </tr>
               </table>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps,null)(OverviewContent);
