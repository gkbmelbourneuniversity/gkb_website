/*
 * HomePage component contains GoogleAutoSuggest, SearchResutList and AutoSuggestList
 * This component connect to redux store as a container component
 */

import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions/authAction';
import { setShowSearchResult } from '../../actions/setShowSearchResult';
import {setContributionArray } from '../../actions/setContributionArray';
import { setGoButtonResultsArray } from '../../actions/goButtonResults'
import { updateShowSearchResult } from '../../actions/updateShowSearchResult';
import { updateContributionArray } from '../../actions/updateContributionArray';
import { updateShowSearchResultLike } from '../../actions/updateShowSearchResultLike';
import ResultPage from './ResultPage';
import AutoSuggestList from './AutoSuggestList';
import GoogleAutoSuggest from '../googleMaps/GoogleAutoSuggest';
import { addToFavoritesAction } from '../../actions/addToFavorites'

class SearchPage extends React.Component {

    constructor(props){
        super(props);

        this.searchString = "";
        this.state = {
            searchString: "",
        }
    }


    render() {
        const { isAuthenticated } = this.props.login;
        const searchResult = this.props.searchResult;
        const descriptionArray = this.props.descriptionArray;
        const goButtonResultsArray = this.props.goButtonResultsArray;

        var searchResultFlag = false
        if (searchResult && searchResult.searchResultPageConfig &&
            searchResult.searchResultPageConfig.showSearchResult == true) {
            searchResultFlag = true
        }

        return (
	    <div className="row">
		<div className="col-lg-12">
                    <div className="row">
                        <div className="col-lg-12 page-up">
                        <GoogleAutoSuggest
                             hideSearchResult={this.props.hideSearchResult}
                             placeholder = {this.props.placeholder}
                             storeSearchString = {this.storeSearchString}
                             setSearchLocation = {this.props.setSearchLocation}
                             closePage = {this.props.closePage}
                             setAutoSuggest = {this.props.setAutoSuggest}
                             hideResultPage = {this.props.hideResultPage}
                             hideCross = {!this.props.showResultPage}
                        />
                        </div>
                    </div>
                    <div className="row">
		        <div className="col-lg-12">
                        {(searchResultFlag && this.props.showResultPage) &&
		             <ResultPage searchResult={searchResult}
                                         isAuthenticated = {isAuthenticated}
                                         descriptionArray = {descriptionArray}
                                         login={this.props.login}
                                         user_id={this.props.login.user._id}
                                         hideSearchResult={this.hideSearchResult}
                                         searchString={this.searchString}
                                         requestDirection={this.props.requestDirection}
                                         setShowDirection={this.props.setShowDirection}
                             /> }
		        </div>
		    </div>

		    <div className="row">
		        <div className="col-lg-12">
                        {!(searchResultFlag && this.props.showResultPage) &&
			   this.props.showAutoSuggest &&
		           goButtonResultsArray['goButtonResultsArray'] &&
		           goButtonResultsArray['goButtonResultsArray'].length > 0 &&
		          <AutoSuggestList goButtonResultsArray={goButtonResultsArray}/>
		        }
		        </div>
		    </div>
		</div>
            </div>
        )
    }
}

SearchPage.propTypes = {
    login: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    console.log("CHECK STATE",state)
    return {
        login: state.login,
        searchResult: state.searchResult,
        descriptionArray: state.descriptionArray,
        goButtonResultsArray: state.goButtonResultsArray,
    };
}

export default connect(mapStateToProps, { logout, })(SearchPage);
