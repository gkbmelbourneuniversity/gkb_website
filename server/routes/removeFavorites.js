/*
 * This route handles favorite requests
 */

import express from 'express';
var User = require('./../models/user.js');
var GooglePlaces = require('./../models/googlePlaces')

let router = express.Router();
router.post('/', (req, res) => {
    var addr = req.body.location
    var id = req.body.user_id

    console.log(addr)
    GooglePlaces.findOneAndRemove({addr: addr})

    User.findOne({_id: id}, function(err, data) {
        if (err || !data) {
            console.log(err);
        } else {
            User.update(
                {_id: id},
                {$pull: { 'favorites' : {searchStr: addr }}},
                function(err, status) {
                    if (err) {
                        console.log(err)
                    } else {
                        User.findOne({_id: id}, function(err, user) {
                        if (!err && user) {
                            var token = {
                                user: user._doc
                            }
                            res.status(200).json({token});
                        }})
                    }
                }
            )
        }
    })
})

export default router;
