/*
 * ResultPage is the container component for SearchResultHead and SearchResultItem
 * It connect to redux store, fetch the newest searchResult State and transfer to SearchResultHead and SearchResultItem
 */

import React from 'react';
import SearchResultItem from './SearchResultItem'
import SearchResultHead from './SearchResultHead'
import addPic from '../img/add-post-button-dark.png';
import AddDescription from './AddDescription';
import { Link } from 'react-router';
import lodash from 'lodash';


class ResultPage extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            showAddDescription: false
        }
        this.onClickAdd = this.onClickAdd.bind(this);
        this.showAddWindow = this.showAddWindow.bind(this);
        this.hideAddWindow = this.hideAddWindow.bind(this);
    }

    onClickAdd(){
        const isAuthenticated = this.props.isAuthenticated;
        if (isAuthenticated){
            this.showAddWindow();
        }
    }

    showAddWindow(){
        this.setState({
            showAddDescription: true
        });
    }

    hideAddWindow(){
        this.setState({
            showAddDescription: false
        });
    }

    render() {
        var items = [];
        const { placeFullAddr } = this.props.searchResult.searchResultPageConfig;
        const { placePhoto } = this.props.searchResult.searchResultPageConfig;
        const { array }= this.props.descriptionArray;
        const isAuthenticated = this.props.isAuthenticated;

        var addImg;
        if(isAuthenticated){
            addImg = <img id = "add" src = {addPic}
                              onClick = {this.onClickAdd}
            />;
        }else {
            addImg = <Link to="/login" ><img id = "add" src = {addPic}/>
                    </Link>;
        }

        if(!lodash.isEmpty(array)){
            const numList = array.length;
            for (var i = 0; i < numList; i++) {
                const user_id = this.props.user_id;
                const {user_like_array} = array[i].doc;
                var thumbUp = false;
                if(user_like_array != undefined){
                    if(user_like_array.indexOf(user_id) != -1){
                        thumbUp = true;
                    }
                }

                items.push(<tr key={i}><td><SearchResultItem userName={array[i].doc.user_name}
                                                             like= {array[i].doc.like}
                                                             num = {i+1}
                                                             des_id = {array[i].doc._id}
                                                             proImg = {array[i].proImg}
                                                             isAuthenticated = {isAuthenticated}
                                                             user_id = {user_id}
                                                             preThumbUp = {thumbUp}
                                                             discription={array[i].doc.description_content}
                                                            />
                    </td></tr>
                );
            }
        }else{
            items.push(<tr key={1}><td><div className = "empty_result_box">
                                    <div className="no-user-submitted-de">No user-submitted descriptions available…</div>
                                    <div className="contribute-your-own">Contribute your own description for this location</div>
                                    <div className="add_box">
                                        {addImg}
                                    </div>
                                </div>
                </td></tr>

            );
        }

        const {autoDescription, sentence} = this.props.login.user;
        console.log('props resultlist',this.props);

        var autoSen;
        if(sentence){
            autoSen = sentence;
        }
        else{
            autoSen = "No sentence is found";
        }

        var autoDesc;
        if (autoDescription) {
          autoDesc = autoDescription;
        } else {
          autoDesc = "No computer generated description available"
        }
	try {
	    var autoComments = autoDesc.match( /[^\.!\?]+[\.!\?]+/g );
	    var wikiSent = autoComments[0];
	    var autoSent = autoComments.slice(1);
	} catch(e) {
	    var wikiSent = autoComments;
	}
	finally{}

        return (
        <div className="row">
            <div className="col-lg-12 no-click-through result_page pre-scrollable scrollbar-primary">
		<div className="row">
		    <div className="col-lg-12">
                        <SearchResultHead
		            autoComment = {autoDesc}
                            location = {placeFullAddr}
                            photo = {placePhoto}
                            addToFavoritesAction = {this.props.addToFavoritesAction}
                            hideSearchResult={this.props.hideSearchResult}
                            searchString={this.props.searchString}
                            requestDirection={this.props.requestDirection}
                            setShowDirection={this.props.setShowDirection}
                        />
		    </div>
		</div>

                <div className="row">
		    <div className="col-lg-12">
		        <div className="row">
		            <div className="col-lg-12 auto-comment">
		                {wikiSent} <b>{autoSent}</b>
		            </div>
		        </div>

		        <div className="row">
		            <div className="col-lg-12 auto-sentence">
		            {autoSen}
		            </div>
		        </div>

                        <div className="row">
		            <div className="col-lg-12 left-text">
                                <span> Location Descriptions </span>
		            </div>
                        </div>

                        <div className="row">
		            <table className="col-lg-12 scrollit scrollbar-primary">
                                 <tbody>{items}</tbody>
                            </table>
                        </div>

		        <div className="row">
		            <div className="col-lg-12">
                            {this.state.showAddDescription &&
		             <AddDescription
		                 hideAddWindow={this.hideAddWindow}
                             />
			    }
		            </div>
		        </div>
		    </div>
		</div>
            </div>
        </div>
        )
    }

}

ResultPage.propTypes = {
    searchResult: React.PropTypes.object.isRequired,
    descriptionArray: React.PropTypes.object.isRequired,
}


export default ResultPage;
