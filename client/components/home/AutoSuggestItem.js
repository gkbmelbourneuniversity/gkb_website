/*
 * All the auto suggested items are rendered in this component
 */

import React from 'react';
import {Link} from 'react-router';
import { searchBarRequest } from '../../actions/searchBarAction';
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setShowSearchResult } from '../../actions/setShowSearchResult';
import { setDescriptionArray }from '../../actions/setDescriptionArray';
import { googlePlaceSearchRequest } from '../../actions/googlePlaceSearch.js';
import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import config from '../../../server/config.js';
import NProgress from '../../actions/nprogress';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class AutoSuggestItem extends React.Component {

  constructor(props){
      super(props);
      this.state = {
          searchStr: "",
          errors: {},
          blocking: false
      }
      this.linkClick = this.linkClick.bind(this);
  }

  linkClick(searchStr, fulladdr, coordinate, imgSrc) {
      const {user} = this.props.login;
      this.setState({errors: {} });
      NProgress.start();
      this.setState({blocking: true });
      var toSend = {
          searchStr: searchStr,
          user_id: user._id,
          fulladdr: fulladdr,
          button: false,
          coord: coordinate
      }

      this.props.searchBarRequest(toSend)
          .then(
              (res) => {
                  const token = res.data.token;
                  console.log('decode token: ',token);
                  NProgress.done();
                  this.setState({blocking: false });

                  this.props.updateCoordsRequest(token);
                  const conf = {
                      showSearchResult: true,
                      placeFullAddr:token.placeFullAddr,
                      placePhoto: imgSrc,
                      type: "jena"
                  }

                  if (token.descriptionArray) {
                      console.log("token descriptionArray line 90 googleAuto");
                      this.props.setDescriptionArray(token.descriptionArray);
                  } else {
                      this.props.setDescriptionArray({});
                  }
                  this.props.setShowSearchResult(conf);
                  browserHistory.push('/home');
              },
              // if server response any error message, set it into state errors
              (err) => {
                  console.log("in google searchHistory")
                  var photo = "";
                  var obj = {
                    searchStr: searchStr
                  }
                  this.props.googlePlaceSearchRequest(obj).then(
                      (res) => {
                          NProgress.done();
                          const data = res.data.obj;
                          console.log("the data we got back from google searchHistory clicked: ", data)
                          user.coords = {lat: data.lat, longt: data.lng};
                          // no direction for this
                          user.directions = null
                          console.log("err  response: ", err.response)
                          if (err.response.data.searchHistory) {
                              console.log("err.response.searchHistory: ", err.response.data.searchHistory)
                              user.searchHistory = err.response.data.searchHistory;
                          }

                          if (err.response.data.autoDescription) {
                              console.log("err.response.autoDescription searchHistory: ", err.response.data.autoDescription)
                              user.autoDescription = err.response.data.autoDescription;
                          } else {
                              user.autoDescription = null
                          }

                          //const descriptionArray = err.response.descriptionArray
                          // Changing thw whole functionality. For now let descriptionArray is null
                          var descriptionArray = err.response.data.descriptionArray;
                          const conf = {
                              showSearchResult: true,
                              placeFullAddr:searchStr,
                              placePhoto: data.photo,
                              type: "google"
                          }
                          console.log("conf conf: ", conf)
                          this.props.setShowSearchResult(conf);

                          if (descriptionArray) {
                              this.props.setDescriptionArray(descriptionArray);
                          } else {
                              this.props.setDescriptionArray({});
                          }
                          this.props.updateCoordsRequest(user);
                          browserHistory.push('/home');
                      },
                      (err) => {
                          console.log("in googlePlaceSearch error client");
                      }
                  )
              }
          );
    }

    render() {
      var imgSrc = this.props.photo
      if(imgSrc == '' || imgSrc == null){
          imgSrc = "http://www.mozmagic.com/files/assets/img/ui/no-image-available.png"
      } else if (imgSrc.indexOf("https") < 0) {
          imgSrc = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=160&photoreference=" + imgSrc + "&key=" + config.googlePlaceApiKey
      }

      var location = this.props.name
      var addr = this.props.addr
      var coord = {title: this.props.name, latitude: this.props.lat, longitude: this.props.lon}

      return(
	  <div className="row">
              <div className="col-lg-12 auto-item">
                  <BlockUi tag="div" blocking={this.state.blocking} className="row"
                       onClick={this.linkClick.bind(this, location, addr, coord, imgSrc)}>
                  <script src='nprogress.js'></script>
                      <img className="col-5 col-lg-5 img-goButton-auto" src={imgSrc}/>
                      <div className="col-7 col-lg-7 auto-item-text">
                          <h4>{this.props.name}</h4>
                          <address>{this.props.addr}</address>
	              </div>
                  </BlockUi>
              </div>
          </div>
      )
    }
}

AutoSuggestItem.propTypes = {
    searchBarRequest: React.PropTypes.func.isRequired,
    login: React.PropTypes.object.isRequired,
    updateCoordsRequest: React.PropTypes.func.isRequired,
    setDescriptionArray: React.PropTypes.func.isRequired,
    setShowSearchResult: React.PropTypes.func.isRequired,
    googlePlaceSearchRequest: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    //console.log('mapStateToPropsCoords: ',state.coords);
    return {
        login: state.login,
        //coords: state.coords
    };
}

export default connect(mapStateToProps, {searchBarRequest,
                                        updateCoordsRequest,
                                        setShowSearchResult,
                                        setDescriptionArray,
                                        googlePlaceSearchRequest})(AutoSuggestItem);
