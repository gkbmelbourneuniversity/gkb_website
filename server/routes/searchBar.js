/*
 * This route handles requests for search a place
 * When the request comes in, the sever will search Apache Jena first, if no result, the sever will seach result from Google.
 */

import express from 'express';
import lodash from 'lodash';
import validator from 'validator';
import config from '../config'
import curl from 'curlrequest';
import jwt from 'jsonwebtoken';
import rest from 'rest';
import ElementEl from './../models/node.js';
import User from './../models/user.js';
import DescriptionSchema from './../models/placeDescription';
import GooglePlaces from './../models/googlePlaces';
import autoDescription from './../models/autoDescription.js';
import sentences from './../models/sentences';

var stringSimilarity = require('string-similarity');

let router = express.Router();

router.post('/', (req, res) => {
    console.log(req.body);
    console.log("id: " + req.body.user_id);
    console.log("searchStr: " + req.body.searchStr);
    console.log("finally in searchBar route");
    console.log("fulladdr: ", req.body.fulladdr);
    console.log("DATE: ", Date());

    const query = {placeFullAddr: req.body.fulladdr}
    const button = req.body.button

    // if button was clicked, different way of dealing
    if (button) {
        console.log("go button clicked")
        var results = [];
        // using google text search api, results in json format
        rest('http://api.ipstack.com/115.146.90.170?access_key=edc42ece0fcfa3836b3a3cdb3d89918b&output=json&legacy=1').then(function (response) {
            var parsedData = JSON.parse(response.entity)
            var pos = {
                lat: parsedData.latitude,
                lng: parsedData.longitude
            };

            var encodeRes = encodeURIComponent(req.body.searchStr)
            var url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + encodeRes + "&location=" + pos.lat + "," + pos.lng + "&radius=20&key=" + config.googlePlaceApiKey
            console.log("url: ", url)
            var options = {url: url};
            var errors = {}

            curl.request(options, function (err, res1) {
                if (err) {
                    errors.searchBar = "google api error, We could not find " + req.body.searchStr
                    res.status(400).json(errors);
                } else {
                    var element = JSON.parse(res1)
                    if (!element.results[0]) {
                        console.log("not in google search")
                        errors.searchBar = "We could not find " + req.body.searchStr
                        res.status(400).json(errors);
                    } else {
                        for (var i = 0; i < element.results.length; i++) {
                            var elem = element.results[i];
                            var lat = element.results[i].geometry.location.lat;
                            var lng = element.results[i].geometry.location.lng;
                            var addr = elem.formatted_address;
                            var name = elem.name;
                            var photo_ref = null
                            if (elem.photos) {
                                photo_ref = elem.photos[0].photo_reference;
                            }

                            var obj = {
                                lat: lat,
                                lng: lng,
                                photo: photo_ref,
                                addr: addr,
                                name: name
                            }
                            results.push(obj)
                            if (results.length == element.results.length) {
                                res.json({results});
                            }
                        }
                    }
                }
            });
        });
    } else {
        console.log("clicked on google Auto suggestion");
        var ret = {};
        ret = queryJena(req.body.searchStr, req.body.fulladdr, req.body.user_id, req.body.coord, function (ret) {
            if (ret.error == 1) {
                console.log("not in jena, but in google");
                var errors = ret.errors;
                const query = {placeFullAddr: req.body.fulladdr}
                User.findById(req.body.user_id, function (err, s_user) {
                    DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                        if (err) return handleError(err);
                        var descriptionArray = [];
                        if (docs.length == 0) {
                            res.status(400).json({
                                errors: null,
                                searchHistory: ret.searchHistory,
                                descriptionArray: null,
                                favorites: null
                            });
                        } else {
                            docs.forEach((doc) => {
                                var temp = {};
                                User.findById(doc.user_id, 'proImg', function (err, user) {
                                    temp.doc = doc;
                                    temp.proImg = user ? user.proImg : null;
                                    descriptionArray.push(temp);
                                });
                            });
                            descriptionArray.sort((a, b) => {
                                if (a.doc.like > b.doc.like) {
                                    return -1;
                                } else if (a.doc.like < b.doc.like) {
                                    return 1;
                                }
                                return 0;
                            });
                            var errors = {
                                errors: ret.errors,
                                searchHistory: ret.searchHistory,
                                descriptionArray: descriptionArray,
                                favorites: ret.favorites
                            }
                            console.log("not in jena, but in google. With descriptionArray", errors)
                            res.status(400).json(errors);
                        }
                    });
                });
            } else {
                var token = ret.token;
                console.log("return from jena");

                const query = {placeFullAddr: token.placeFullAddr};
                User.findById(req.body.user_id, function (err, s_user) {
                    DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                        if (err) return handleError(err);
                        var descriptionArray = [];

                        if (docs.length == 0) {
                            console.log(" in jena, Without descriptionArray")
                            token.descriptionArray = null
                            res.json({token});
                        } else {
                            docs.forEach((doc) => {
                                var temp = {};
                                User.findById(doc.user_id, 'proImg', function (err, user) {
                                    temp.doc = doc;
                                    temp.proImg = user ? user.proImg : null;
                                    descriptionArray.push(temp);
                                });
                            });

                            descriptionArray.sort((a, b) => {
                                if (a.doc.like > b.doc.like) {
                                    return -1;
                                } else if (a.doc.like < b.doc.like) {
                                    return 1;
                                }
                                return 0;
                            });

                            console.log(" in jena, With descriptionArray");
                            token.descriptionArray = descriptionArray;
                            res.json({token});
                        }
                    });
                });
            }
        });
    }
});


function querySentence(placeid , temp , callback ){

    var sentence;
    //return query;
    temp = sentence;
    console.log("inside temp", temp);

    sentences.findOne({'properties.om_id': placeid}, function (err, res2) {
        if (err) {
            console.log("err in sentences findone");
            console.log("error ", err);
        }
        else {
            if (res2) {
                sentence = res2.token;
                temp = res2.token;
                console.log("res sentences", res2);
            }
            else {
                return null;
                console.log("in sentencce err");
            }
        }
        const token ={
            sentence: res2.token
        }
        var ret = {
            error: 0,
            token: token
        }

        callback(ret);
    });


}

function callNominatim(searchStr, fulladdr, coord, callback){
    if (!coord) {
	callback("[]")
	return
    }

    var lat = coord['latitude']
    var lon = coord['longitude']
    var temp = 'http://45.113.232.47/nominatim/search?bounded=1&viewbox='+(lon-0.001)+','+(lat-0.001)+','+(lon+0.001)+','+(lat+0.001)+'&q='
    var temp2 = '&format=json&addressdetails=1'
    var encodeRes = encodeURIComponent(searchStr)
    var url = temp + encodeRes + temp2
    var options = {url: url};

    console.log("LAT: " + lat);
    console.log("LON: " + lon);
    console.log("TITLE: " + coord['title']);
    console.log("encodeRes: " + encodeRes)
    console.log("url: " + url);

    curl.request(options, function (err, res1) {
        if(err){
            console.log("in err curl req");
        }
        else{
            callback(res1);
        }
    });
}

function queryJena(searchStr, fulladdr, id, coord,callback) {
    var nominatim_result = null
    callNominatim(searchStr, fulladdr, coord,function(ret){
        nominatim_result = ret;
        console.log("nominatim result");
        console.log(nominatim_result);
        let errors = {};
        var element = JSON.parse(nominatim_result)
        if (!element[0]) {
            console.log("not in nominatim jena service")
            errors.searchBar = "We could not find " + fulladdr
            // Then store the searchstr in searchStr in db with google type.
            if (id != null) {
                User.findOne({_id: id}, function (err, data2) {
                    let errors = {};
                    var searchHistoryStore = [];
                    searchHistoryStore = data2.searchHistory;
                    if (err) {
                        console.log(err);
                    } else if (!data2) {
                    } else {
                        var insertToSearchHistory = {
                            type: "google",
                            searchStr: fulladdr,
                        }
                        //Update searchHistory in user Model.
                        User.update(
                            {_id: id, searchHistory: insertToSearchHistory},
                            {$addToSet: {searchHistory: insertToSearchHistory}},
                            function (err, user) {
                                if (err) {
                                    console.log("error in searchhistory update");
                                } else {
                                    User.findOne({
                                        _id: id,
                                        'searchHistory.searchStr': fulladdr
                                    }, function (err, data) {
                                        if (err) {
                                        } else if (!data) {
                                            var insertToSearchHistoryNew = {
                                                type: "google",
                                                searchStr: fulladdr,
                                                date: new Date()
                                            }
                                            User.update(
                                                {_id: id},
                                                {$addToSet: {searchHistory: insertToSearchHistoryNew}},
                                                function (err, user) {
                                                    if (err) {
                                                        console.log("in 2nd update error")
                                                    } else {
                                                        searchHistoryStore.push(insertToSearchHistoryNew);
                                                        var ret = {
                                                            error: 1,
                                                            errors: errors,
                                                            searchHistory: searchHistoryStore,
                                                            autoDescription: null,
                                                            favorites: data2.favorites,
                                                            osm_id:nominatim_result.place_id,
                                                            osm_type: nominatim_result.osm_type
                                                        }
                                                        callback(ret);
                                                    }
                                                })
                                        } else {
                                            User.update(
                                                {'searchHistory.searchStr': fulladdr},
                                                {$set: {'searchHistory.$.date': new Date()}},
                                                function (err, user2) {
                                                    if (err) {
                                                        console.log("error date updated");
                                                    } else {
                                                        console.log("updating date", user2);
                                                        var ret = {
                                                            error: 1,
                                                            errors: errors,
                                                            searchHistory: data2.searchHistory,
                                                            autoDescription: null,
                                                            favorites: data2.favorites,
                                                            osm_id:nominatim_result.place_id,
                                                            osm_type: nominatim_result.osm_type

                                                        }
                                                        callback(ret);
                                                    }
                                                })
                                        }
                                    })
                                }
                            });
                    }
                });
            } else {
                // no user. Guest user trying to search
                errors.searchBar = "We could not find " + searchStr
                var ret = {
                    error: 1,
                    errors: errors,
                    searchHistory: null,
                    autoDescription: null,
                    favorites: null
                }
                callback(ret);
            }
        } else {
            var compare = searchStr + " " + fulladdr;
            var bestMatch = stringSimilarity.compareTwoStrings(element[0].display_name, compare);
            var idxBest = 0
            for (var i = 1; i < element.length; i++) {
                var tmp = stringSimilarity.compareTwoStrings(element[i].display_name, compare);
                if (tmp > bestMatch) {
                    bestMatch = tmp;
                    idxBest = i;
                }
            }

            console.log("jena success");
            var elem = element[idxBest]
            fulladdr = elem.display_name;

            autoDescription.findOne({'element': elem.osm_id}, function (err, res) {
                if (err) {
                    console.log("err in autoDescription findone")
                } else {
                    var autoDescription;

                    if (res) {
                        console.log("res autoDescription:",res);
                        autoDescription = res.autoDescription ;
                        console.log("in doc success")
                    } else {
                        autoDescription = null;
                        console.log("in doc err")
                    }
                    if (id != null) {
                        User.findOne({_id: id}, function (err, data2) {
                            var searchHistoryStore = [];
                            let errors = {};
                            searchHistoryStore = data2.searchHistory;
                            if (err) {
                                console.log(err);
                            } else if (!data2) {
                            } else {
                                var coords2 = {
                                    lat: elem.lat,
                                    longt: elem.lon
                                }

                                var insertToSearchHistory = {
                                    type: "jena",
                                    element: elem.osm_id,
                                    searchStr: fulladdr,
                                }
                                //Update searchHistory in user Model.
                                User.update(
                                    {_id: id, searchHistory: insertToSearchHistory},
                                    {$addToSet: {searchHistory: insertToSearchHistory}},
                                    function (err, user) {
                                        if (err) {
                                            console.log("error in searchhistory update");
                                        } else {
                                            User.findOne({
                                                _id: id,
                                                'searchHistory.searchStr': fulladdr
                                            }, function (err, data) {
                                                if (err) {
                                                } else if (!data) {
                                                    var insertToSearchHistoryNew = {
                                                        type: "jena",
                                                        element: elem.osm_id,
                                                        searchStr: fulladdr,
                                                        date: new Date()
                                                    }
                                                    User.update(
                                                        {_id: id},
                                                        {$addToSet: {searchHistory: insertToSearchHistoryNew}},
                                                        function (err, user) {
                                                            if (err) {
                                                                console.log("in 2nd update error")
                                                            } else {
                                                                searchHistoryStore.push(insertToSearchHistoryNew);
                                                                const token = {
                                                                    email: data2.email,
                                                                    userName: data2.userName,
                                                                    accountType: data2.accountType,
                                                                    _id: data2._id,
                                                                    proImg: data2.proImg,
                                                                    coords: coords2,
                                                                    placeFullAddr: fulladdr,
                                                                    placePhoto: "",
                                                                    searchHistory: searchHistoryStore,
                                                                    autoDescription: autoDescription,
                                                                    favorites: data2.favorites,
				                                    country: elem.address.country,
                                                                    osm_id:elem.osm_id,
                                                                    osm_type: elem.osm_type
                                                                }
                                                                var ret = {
                                                                    error: 0,
                                                                    token: token
                                                                }
                                                                callback(ret);
                                                            }
                                                        })

                                                } else {
                                                    User.update(
                                                        {'searchHistory.searchStr': fulladdr},
                                                        {$set: {'searchHistory.$.date': new Date()}},
                                                        function (err, user2) {
                                                            if (err) {
                                                                console.log("error date updated");
                                                            } else {
                                                                const token = {
                                                                    email: data2.email,
                                                                    userName: data2.userName,
                                                                    accountType: data2.accountType,
                                                                    _id: data2._id,
                                                                    proImg: data2.proImg,
                                                                    coords: coords2,
                                                                    placeFullAddr: fulladdr,
                                                                    placePhoto: "",
                                                                    searchHistory: data2.searchHistory,
                                                                    autoDescription: autoDescription,
                                                                    favorites: data2.favorites,
				                                    country: elem.address.country,
                                                                    osm_id:elem.osm_id,
                                                                    osm_type: elem.osm_type
                                                                }
                                                                console.log("search bar sending token ");
                                                                var ret = {
                                                                    error: 0,
                                                                    token: token
                                                                }
                                                                callback(ret);
                                                            }
                                                        })
                                                }
                                            })
                                        }
                                    });
                            }
                        });
                    } else {
                        // add sentence

                        var coords2 = {
                            lat: elem.lat,
                            longt: elem.lon
                        }
                        console.log("coords2: ", coords2);

                        sentences.findOne({'properties.om_id': elem.osm_id}, function (err, data3) {
                            if (err) {
                                console.log("err in sentences findone");
                                console.log("error ", err);
                                console.log("sentence res", data3);

                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    favorites: null,
				    country: elem.address.country,
                                    osm_id:elem.osm_id,
                                    osm_type: elem.osm_type
                                }
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }
                            else if (!data3){
                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    sentence: null,
                                    favorites: null,
				    country: elem.address.country,
                                    osm_id:elem.osm_id,
                                    osm_type:elem.osm_type
                                }
                                console.log("search bar sending token2 ");
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }
                            else {
                                var autoSentence;
                                if(autoDescription.includes(data3.properties.sentences[0])){
                                    console.log("sentence is included");
                                    if(data3.properties.sentences.length >= 2){
                                        autoSentence = data3.properties.sentences[1];
                                    }
                                    else {
                                        autoSentence = null;
                                    }
                                }
                                else {
                                    autoSentence = data3.properties.sentences[0];

                                }

                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    sentence: autoSentence,
                                    favorites: null,
				    country: elem.address.country,
                                    osm_id:elem.osm_id,
                                    osm_type: elem.osm_type
                                }
                                console.log("search bar sending new token2 ");
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }
                        });
                    }
                }
            });

        }
    });
}

// handle search_bar location search
// test version
router.get('/testgo', (req, res) => {
    const resultlist = loadResultList(req.query.location);
    console.log(resultlist);
    res.json(resultlist);
});

router.post('/addDescription', (req, res) => {
    // Add this place to googlePlaces in db only if they type is google
    if (req.body.type == "google") {
        console.log("in addDescription google places add");

        var query = {
            addr: req.body.placeFullAddr,
        }

        var place = {
            addr: req.body.placeFullAddr,
            image: req.body.image,
            coords: req.body.coords,
        }

        GooglePlaces.find(query).count(function (err, count) {
            let errors = {}
            console.log("In addFavorites Number of docs: ", count);
            if (count === 0) {
                GooglePlaces.create(place, function (err, dataGoogle) {
                    console.log("Writing to db");
                    if (err) {
                        console.log(err.statusCode);
                    } else if (!dataGoogle) {
                        console.log(res.statusCode);
                        console.log("Error saving");
                    } else {
                        console.log(res.statusCode);
                        console.log("Registered");
                    }
                });
            } else {
                console.log("place already exists in Favorites db");
            }
        });
    }

    var description = req.body
    console.log("descriptionis gonna be stored.: ", description)
    description.like = 0;
    description.date = Date();

    DescriptionSchema.create(description, function (err, data) {
        console.log("Writing to db");
        console.log(description);
        if (err) {
            console.log(err.statusCode);
            console.log(err);
            console.log("get error");
        } else if (!data) {
            console.log(res.statusCode);
            console.log("Error saving");
        } else {
            const query = {placeFullAddr: description.placeFullAddr};
            DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                if (err) console.log(err);
                var counter = 1
                var descriptionArray = [];
                docs.forEach((doc) => {
                    var temp = {};
                    User.findById(doc.user_id, 'proImg', function (err, user) {
                        temp.doc = doc;
                        console.log("user: ***************", user);
                        if (!user || user.proImg == null) {
                            temp.proImg = null
                        } else {
                            temp.proImg = user.proImg;
                        }
                        descriptionArray.push(temp);
                        if (counter == docs.length) {
                            descriptionArray.sort((a, b) => {
                                if (a.doc.like > b.doc.like) {
                                    return -1;
                                } else if (a.doc.like < b.doc.like) {
                                    return 1;
                                }
                                return 0;
                            });

                            ////////////////////////////////////
                            ////Update contribution here////////
                            ////////////////////////////////////

                            var this_user = {
                                user_id: description.user_id,
                                user_name: description.user_name
                            }
                            console.log("Im this user:" + JSON.stringify(description));

                            DescriptionSchema.find(this_user, function (err, data) {
                                var addresses = [];
                                if (err) {
                                    console.log(err);
                                } else if (!data) {
                                    console.log("No contribution yet");
                                } else {
                                    console.log("Descriptions: " + data[0]);
                                    var user_descriptions = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var this_description = {
                                            location: data[i].placeFullAddr,
                                            description: data[i].description_content,
                                            create_date: data[i].date
                                        }
                                        addresses.push(data[i].placeFullAddr);
                                        user_descriptions.push(this_description);
                                    }

                                }


                                GooglePlaces.find({addr: {$in: addresses}}, function (err, data) {
                                    if (err) {
                                        console.log("Error finding google places " + err);
                                    } else if (!data) {
                                        console.log("Cannot find in googleplaces");
                                    } else {
                                        console.log("MATCHING google palce " + data.length);
                                        for (var i = 0; i < data.length; i++) {
                                            for (var j = 0; j < user_descriptions.length; j++) {
                                                if (data[i].addr === user_descriptions[j].location) {
                                                    user_descriptions[i].image = data[i].image;
                                                }
                                            }

                                        }
                                    }


                                    res.status(200).json({
                                        descriptionArray: descriptionArray,
                                        contributionArray: user_descriptions
                                    });

                                });
                            });
                        }
                        counter += 1;
                    });
                });
            });
        }
    })
});

router.post('/updateContribution', (req, res) => {
    console.log("UPDATING CONTRIBUTION ARRAY IN HERE 1");
    res.status(200).json({});

});


router.post('/addLike', (req, res) => {
    const {des_id} = req.body;
    const {user_id} = req.body;
    // check if the user already liked this one
    var liked = false;
    DescriptionSchema.findById(des_id, function (err, description) {
        const {user_like_array} = description;
        console.log(description);
        var response = {};
        if (user_like_array.indexOf(user_id) === -1) {
            // user haven't like this one
            DescriptionSchema.findByIdAndUpdate(des_id, {
                $inc: {like: 1},
                $push: {user_like_array: user_id}
            }, {new: true}, function (err, description) {
                if (err) return handleError(err);
                const query = {placeFullAddr: description.placeFullAddr};
                console.log("query", query);
                DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                    if (err) console.log(err);
                    var counter = 1
                    var descriptionArray = [];
                    docs.forEach((doc) => {
                        var temp = {};
                        User.findById(doc.user_id, 'proImg', function (err, user) {
                            temp.doc = doc;
                            if (!user || user.proImg == null) {
                                temp.proImg = null
                            } else {
                                temp.proImg = user.proImg;
                            }
                            descriptionArray.push(temp);
                            if (counter == docs.length) {
                                descriptionArray.sort((a, b) => {
                                    if (a.doc.like > b.doc.like) {
                                        return -1;
                                    } else if (a.doc.like < b.doc.like) {
                                        return 1;
                                    }
                                    return 0;
                                });
                                console.log("return with description array")
                                response.descriptionArray = descriptionArray
                                res.status(200).json(response);
                            }
                            counter += 1;
                        });
                    });
                });
            });
        } else {
            // user already liked this one
            response.ans = false;
            // return refused as signal
            res.status(200).json(response);
        }
    });

});


// simulate load query result from db
function loadResultList(location) {
    const results = {};
    const resultNum = rnd(3, 8);
    const resultArray = []
    for (let i = 0; i < resultNum; i++) {
        let tempJson = {};
        tempJson.userName = 'user-' + i;
        tempJson.rank = rnd(0, 300);
        tempJson.discription = 'rank' + i + 'xxxxxxxxxxxxxxxxxx';
        resultArray.push(tempJson);
    }
    resultArray.sort(function (a, b) {
        return parseInt(b.rank) - parseInt(a.rank);
    });
    results.location = location;
    results.resultArray = resultArray;
    results.autoComment = 'autoComment is here';

    return results;
}

// produce a random number
function rnd(start, end) {
    return Math.floor(Math.random() * (end - start) + start);
}

//we need to get data from post request

export default router;
