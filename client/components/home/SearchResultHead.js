/*
 * SearchResultHead is a UI component, only render the data sent from SearchResultList
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import place from '../img/ic-place-black-48-dp.png';
import favour_icon from '../img/icons8-heart-filled-50.png';
import share_icon from '../img/icons8-share-filled-50.png';
import nearby_icon from '../img/icons8-mind-map-50.png';
import direction_icon from '../img/icons8-waypoint-map-filled-50.png';
import {Link} from 'react-router';
import config from '../../../server/config.js';
import Nearby from "../navigation/Nearby";
import Share from '../navigation/Share';
import default_image from '../img/no-image-available.png';
import { addToFavoritesAction } from '../../actions/addToFavorites'

class SearchResultHead extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            searchStr: "",
            landmarks: [],
            showShare: false,
	    showNearby: false,
        }

        this.addFavorite = this.addFavorite.bind(this)
	this.pushFavorite = this.pushFavorite.bind(this)
	this.showDirectionSearch = this.showDirectionSearch.bind(this)
    }

    showDirectionSearch() {
        this.props.setShowDirection(true)
    }

    toggleShare() {
        this.setState({ showShare: !this.state.showShare})
    }

    nearby(e){
      var osm_id = this.props.login.user.osm_id;
      var osm_type = this.props.login.user.osm_type;

      if (!this.state.showNearby) {
         $.ajax({
             url: 'http://115.146.90.170:8080/nearestLandmarks?location='+ osm_type +':'+ osm_id,
             data: {
                 format: 'json'
             },
             error: function(xhr,status,error) {
                 var err = eval("("+xhr.responseText+ ")");
             },
             context:this,
                 success: function(data) {
                     this.setState({ landmarks: data.landmarks, showNearby: true })
                     ReactDOM.findDOMNode(this.refs.nearbyView).scrollIntoView()
             },
             type: 'GET'
         });
      } else {
	 this.setState({showNearby: false})
      }
    }

    pushFavorite(amenity) {
        const {user} = this.props.login;
	const { place_id } = this.props.searchResult.searchResultPageConfig;

        var description = {
            amenity : amenity,
            location : this.props.location,
            photo : this.props.photo,
            coords: user.coords,
            user_id: user._id,
            country: user.country,
	    place_id: place_id,
            type: this.props.searchResult.searchResultPageConfig.type,
            autoComment: this.props.autoComment,
        }

        this.props.addToFavoritesAction(user, description).then(
            (res) => {
                console.log("Add favorite successfully");
            },
            (err) => {
                console.log(err);
            });
    }

    addFavorite(e) {
        const {user} = this.props.login;
        var osm_id = user.osm_id;
        var osm_type = user.osm_type;

        $.ajax({
		url: 'http://45.113.232.47/api/interpreter?data=[out:json];'+osm_type+'('+osm_id+');out;',
         data: {
            format: 'json'
         },
         context:this,
         success: function(data) {
            var amenity = data["elements"].length > 0 ? data["elements"][0]["tags"]["amenity"] : null
            this.pushFavorite(amenity)
         },
         error: function(xhr,status,error) {
            this.pushFavorite(null)
         },
         type: 'GET'
      });
    }

    render(){
        const location = this.props.location;
	const { place_id } = this.props.searchResult.searchResultPageConfig;

        var imgSrc = this.props.photo;
        if(imgSrc == '' || imgSrc == null){
            imgSrc = default_image
        } else if (imgSrc.indexOf("https") < 0) {
            // result is from google place photo => photo_ref
            imgSrc = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + imgSrc + "&key=" + config.googlePlaceApiKey
        }

        return(
        <div className="row">
            <div id="search-result-modal" className="col-lg-12">
		<div className="row">
		    <div className="col-lg-12 photo-tag">
		        <div className="row">
                            <img className="col-md-12 col-lg-12" style={{height: '268px'}} src={imgSrc}/>
                            <div className="col-md-12 col-lg-12 place-sec" id="style-1">
                                <img className="small-icon-rec icon-place" src={place}/>
                                <p>{location}</p>
                            </div>
                        </div>

		        <div className="row features-btn">
		            <button type="button"
                                onClick={this.addFavorite}
                                className="btn btn-primary col-xs-12 col-sm-12 col-md-6">
                                <img className="small-icon-sq" src={favour_icon} />
                                Favourite
		           </button>
		           <button type="button"
                                onClick={this.toggleShare.bind(this)}
                                className="btn btn-primary col-xs-12 col-sm-12 col-md-6">
                                <img className="small-icon-sq" src={share_icon}/>
                                Share
		           </button>
		           <button type="button"
                                onClick={this.nearby.bind(this)}
                                className="btn btn-primary col-xs-12 col-sm-12 col-md-6">
                                <img className="small-icon-sq" src={nearby_icon}/>
                                Nearby
		           </button>
		           <button type="button"
                                onClick={this.showDirectionSearch}
                                className="btn btn-primary col-xs-12 col-sm-12 col-md-6">
                                <img className="small-icon-sq" src={direction_icon}/>
                                Direction
		           </button>
		        </div>
                    </div>
                </div>

                <div className="row">
		    <div className="col-lg-12">
                        {this.state.showShare &&
                        <Share place_id={place_id}/>
			}
		    </div>
                </div>

                <div className="row">
		    <div className="col-lg-12">
		    { this.state.showNearby &&
		      < Nearby
                        ref="nearbyView"
		    	requestDirection = {this.props.requestDirection}
                        landmarks = {this.state.landmarks}
		      />
		    }
		    </div>
                </div>
            </div>
        </div>
        );
    }
}

SearchResultHead.propTypes = {
    login: React.PropTypes.object.isRequired,
}

SearchResultHead.contextTypes = {
    router: React.PropTypes.object.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
        login: state.login,
        searchResult: state.searchResult
    };
}

export default connect(mapStateToProps, {addToFavoritesAction})(SearchResultHead);
