/*
 * This component renders the list of favourite items, which is forme by a list of single favourite item
 * This is used by favourite page
 */

import React from 'react';
import FavouriteItem from './FavouriteItem';
import {connect} from 'react-redux';

class FavouriteList extends React.Component{
    constructor(props){
        super(props);

	this.isShown = this.isShown.bind(this)
    }

    isShown(item) {
        var ret = false
	if (this.props.amenity == 'all' || this.props.amenity == item.amenity
		|| (this.props.amenity == 'others' && !(item.amenity))) {
	    if (this.props.country == 'all' || this.props.country == item.country
		    || (this.props.country == 'others' && !(item.country))) {
		ret = true
	    }
	}
	if (this.props.searchStr != "") {
            var filter = this.props.searchStr.toLowerCase()
	    if (item.searchStr.toLowerCase().indexOf(filter) < 0) {
		ret = false
	    }
	}
	return ret
    }

    render() {
        const {user} = this.props.login;
        var items = [];
        var {favorites} = this.props.login.user;

        if (typeof(favorites) != 'undefined') {
            if (this.props.order == 'new') {
                favorites.sort(function(a,b) {return (a.date > b.date) ? -1 : ((b.date > a.date) ? 1 : 0);} );
            } else {
                favorites.sort(function(a,b) {return (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0);} );
            }

            for (var i = 0; i < favorites.length; i++) {
		if (this.isShown(favorites[i])) {
                    items.push(
                        <div key={favorites[i].date} className="col-12">
                            <FavouriteItem location={favorites[i].searchStr}
                                           img={favorites[i].image}
                                           place_id={favorites[i].place_id}
                                           description={favorites[i].description}/>
                        </div>
                    )
		}
            }
        }else{
            console.log("Empty array");
        }

        return(
            <div className="row">
		<div className="col-12">
                {items}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps,null)(FavouriteList);
