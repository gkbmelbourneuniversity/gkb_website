/*
 * This component renders favourite page
 * This is used by profile
 */
import React from 'react';
import {connect} from 'react-redux';
import { ButtonToolbar, DropdownButton, MenuItem } from 'react-bootstrap';
import LinkToHome from '../common/LinkToHome';
import FavouriteList from './FavouriteList';

class FavouritesPage extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            order: "new",
	    country: "all",
            amenity: "all",
	    searchStr: "",
        }
        this.isActive = this.isActive.bind(this)
        this.changeOrder = this.changeOrder.bind(this)
	this.getAmenityList = this.getAmenityList.bind(this)
	this.getCountryList = this.getCountryList.bind(this)
	this.search = this.search.bind(this)
    }

    isActive(value) {
        var ret = "list-group-item list-group-item-white col-12 "
        if (value == this.state.order) {
            ret += "active"
        }
        return ret
    }

    changeOrder(value) {
        this.setState({ order: value})
    }

    search(evt) {
	this.setState({ searchStr: evt.target.value })
    }

    getAmenityList() {
        const{favorites} = this.props.login.user
	var list = []
	var ret = []

	favorites.forEach((item) => {
            if (item["amenity"]) {
	        list.push(item["amenity"])
            }
	})
	list = list.filter(function(item, pos) {
	    return list.indexOf(item) == pos;
	})

	list.forEach((item) => {
            ret.push(
		<MenuItem eventKey={item} key={item}>
		    {item}
		</MenuItem>
            )
	})
        ret.push(<MenuItem eventKey='others' key='others'>others</MenuItem>)
	return ret
    }

    getCountryList() {
        const {favorites} = this.props.login.user
	var list = []
	var ret = []

	favorites.forEach((item) => {
            if (item["country"]) {
		list.push(item["country"])
	    }
	})
	list = list.filter(function(item, pos) {
	    return list.indexOf(item) == pos;
	})

	list.forEach((item) => {
            ret.push(
	        <MenuItem eventKey={item} key={item}>
		    {item}
	        </MenuItem>
	    )
	})
        ret.push(<MenuItem eventKey='others' key='others'>others</MenuItem>)
	return ret
    }

    render(){
        return(
        <div className="vertical-block col-md-offset-1 col-md-8 window-drop-shadow">
            <div className="vertical-block-title">
                <h4>My Favourites</h4>
                <LinkToHome/>
            </div>
            <div className="container vertical-block-content">
	        <div className="row">
                    <div className="vertical-block-content-left col-md-3 col-xs-12">
                        <div className="list-group">
                            <br/>
                            <div className="input-group">
                                <input type="search" className="form-control"
		                       onChange={this.search}
		                       aria-describedby="basic-addon"/>
                                <span className="input-group-addon" id="basic-addon">
                                    <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                            </div>
                            <div className="mid-block-favourite-left">
                                <h5><b>Sort By</b></h5>
                                <a href="#" onClick={() => this.changeOrder('new')}
		                            className={this.isActive('new')}>Newest</a>
                                <a href="#" onClick={() => this.changeOrder('old')}
		                            className={this.isActive('old')}>Oldest</a>
                                <br/>
                                <h5><b>Filter By</b></h5>
		                <DropdownButton
		                    className="list-group-item list-group-item-white"
		                    title="Country" id="country"
		                    onSelect={(evt) => {this.setState({country: evt})}}
		                >
		                    <MenuItem eventKey="all" key='all'>all</MenuItem>
		                    <MenuItem divider />
		                    {this.getCountryList()}
		                </DropdownButton>
		                <DropdownButton
		                    className="list-group-item list-group-item-white"
		                    title="Location Type" id="location_type"
		                    onSelect={(evt) => {this.setState({amenity: evt})}}
		                >
		                    <MenuItem eventKey="all" key='all'>all</MenuItem>
		                    <MenuItem divider />
		                    {this.getAmenityList()}
		                </DropdownButton>
                            </div>
                        </div>
                    </div>
                    <div className="vertical-block-content-right col-md-9 col-xs-12">
                        <FavouriteList order={this.state.order}
		                       country={this.state.country}
		                       amenity={this.state.amenity}
		                       searchStr={this.state.searchStr}
		        />
                   </div>
                </div>
            </div>
        </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, null)(FavouritesPage);
