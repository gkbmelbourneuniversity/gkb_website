/*
 * This component is responsible for forming a single contribution entry
 * This is used by contribution list
 */

import React from 'react';
import { Link } from 'react-router';
import { searchBarRequest } from '../../actions/searchBarAction';
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setShowSearchResult } from '../../actions/setShowSearchResult';
import { setDescriptionArray }from '../../actions/setDescriptionArray';
import { googlePlaceSearchRequest } from '../../actions/googlePlaceSearch.js';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import moment from 'moment';
import config from '../../../server/config.js';


class ContributionItem extends React.Component{

  constructor(props){
      super(props);
      this.linkClick = this.linkClick.bind(this);
  }

  linkClick(searchStr) {
      console.log("link click in searchHistory")

      const {user} = this.props.login;
      var toSend = {
          searchStr: "",
          user_id: user._id,
          fulladdr: searchStr,
          button: false
      }
      this.props.searchBarRequest(toSend)
          .then(
              (res) => {
                  const userData = res.data.token
                  this.props.updateCoordsRequest(userData);
                  const conf = {
                      showSearchResult: true,
                      placeFullAddr:userData.placeFullAddr,
                      placePhoto: "",
                      type: "jena"
                  }
                  if (token.descriptionArray) {
                      console.log("token descriptionArray line 110 googleAuto");
                      this.props.setDescriptionArray(token.descriptionArray);
                  } else {
                      this.props.setDescriptionArray({});
                  }
                  this.props.setShowSearchResult(conf);
                  browserHistory.push('/home');
              },
              (err) => {
                  console.log("in google searchHistory")
                  var photo = "";
                  var obj = {
                    searchStr: searchStr
                  }
                  this.props.googlePlaceSearchRequest(obj).then(
                      (res) => {
                          const data = res.data.obj;
                          user.coords = {lat: data.lat, longt: data.lng};
                          // no direction for this
                          user.directions = null
                          console.log("err  response: ", err.response)
                          if (err.response.data.searchHistory) {
                              console.log("err.response.searchHistory: ", err.response.data.searchHistory)
                              user.searchHistory = err.response.data.searchHistory;
                          }
                          if (err.response.data.autoDescription) {
                              console.log("err.response.autoDescription searchHistory: ", err.response.data.autoDescription)
                              user.autoDescription = err.response.data.autoDescription;
                          } else {
                              user.autoDescription = null
                          }

                          // Changing thw whole functionality. For now let descriptionArray is null
                          var descriptionArray = err.response.data.descriptionArray;
                          const conf = {
                              showSearchResult: true,
                              placeFullAddr:searchStr,
                              placePhoto: data.photo,
                              type: "google"
                          }
                          this.props.setShowSearchResult(conf);
                          if (descriptionArray) {
                              this.props.setDescriptionArray(descriptionArray);
                          } else {
                              this.props.setDescriptionArray({});
                          }
                          this.props.updateCoordsRequest(user);
                          browserHistory.push('/home');
                      },
                      (err) => {
                          console.log("in googlePlaceSearch error client");
                      }
                  )
              }
          );
    }

    render(){
        var {img} = this.props;
        if(img == '' || img == null){
            img = "http://www.mozmagic.com/files/assets/img/ui/no-image-available.png"
        } else if (img.indexOf("https") < 0) {
            // result is from google place photo => photo_ref
            img = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + img + "&key=" + config.googlePlaceApiKey
        }

        var formattedDate = moment(this.props.date).format('MMMM Do YYYY');
        return(
	<div className="row">
            <div className="favourite-item-box"
                 onClick={this.linkClick.bind(this, this.props.location)} >
                <img className="favourite-img col-md-3 col-xs-12" src={img}/>
                <div className="col-md-9 col-xs-12 favourite-content">
                     <h5>{this.props.location}</h5>
                     <h6 className="favourite-content-date">{formattedDate}</h6>
                     <h6>{this.props.description}</h6>
                </div>
            </div>
        </div>
        );
    }
}

ContributionItem.propTypes = {
    searchBarRequest: React.PropTypes.func.isRequired,
    login: React.PropTypes.object.isRequired,
    updateCoordsRequest: React.PropTypes.func.isRequired,
    setDescriptionArray: React.PropTypes.func.isRequired,
    setShowSearchResult: React.PropTypes.func.isRequired,
    googlePlaceSearchRequest: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, {searchBarRequest,
                                        updateCoordsRequest,
                                        setShowSearchResult,
                                        setDescriptionArray,
                                        googlePlaceSearchRequest})(ContributionItem);
