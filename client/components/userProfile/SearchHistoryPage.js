/*
 * This component renders components to form a complete search history page
 * This is used by user profile
 */

import React from 'react';
import LinkToHome from '../common/LinkToHome';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import SearchHistoryList from './SearchHistoryList';

class SearchHistoryPage extends React.Component{
    render(){
        return(
            <div className="vertical-block col-md-offset-1 col-md-8 window-drop-shadow">
                <div className="vertical-block-title">
                    <h4>Search History</h4>
                    <LinkToHome/>
                </div>
                <div className="vertical-block-content container">
                    <div className="history-button">
                        Delete
                    </div>
                    <div  className="history-button">
                        Add to Favourites
                    </div>
                    <div>
                        <SearchHistoryList searchHistory={this.props.searchHistory} />
                    </div>
                </div>
            </div>
        );
    }
}

SearchHistoryPage.propTypes = {
    searchHistory: React.PropTypes.array.isRequired,
}

function mapStateToProps(state) {
    return {
	searchHistory: state.login.user.searchHistory,
        login: state.login,
    };
}

export default connect(mapStateToProps, {})(SearchHistoryPage);
