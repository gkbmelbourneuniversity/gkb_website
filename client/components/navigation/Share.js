/**
 * @file Component to share landmark
 */

import React from 'react';
import { TwitterIcon, GooglePlusIcon, LinkedinIcon } from 'react-share';
import { TwitterButton, GooglePlusButton, LinkedInButton } from 'react-social';

class Share extends React.Component {

    constructor(props) {
        super(props);

        var url = window.location.href.replace("home", "place/")
        url = url.replace("#", "") + this.props.place_id;
        this.state = {
            url : url
        }
    }

    render() {
        return (
	    <div className="row">
                <TwitterButton url={this.state.url}>
                    <TwitterIcon size={60} />
                </TwitterButton>
                <GooglePlusButton url={this.state.url}>
                    <GooglePlusIcon size={60} />
                </GooglePlusButton>
                <LinkedInButton url={this.state.url}>
                    <LinkedinIcon size={60} />
                </LinkedInButton>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        login: state.login,
    };
}

export default Share;
