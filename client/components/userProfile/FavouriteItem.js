/*
 * This component forms a single favourite location entry
 * This is used by favourite list
 */

import React from 'react';
import {Link} from 'react-router';
import NProgress from '../../actions/nprogress';
import { searchBarRequest } from '../../actions/searchBarAction';
import { updateCoordsRequest} from '../../actions/updateCoords';
import { setShowSearchResult } from '../../actions/setShowSearchResult';
import { setDescriptionArray }from '../../actions/setDescriptionArray';
import { googlePlaceSearchRequest } from '../../actions/googlePlaceSearch.js';
import { removeFavoritesAction } from '../../actions/addToFavorites.js';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import config from '../../../server/config.js';

class FavouriteItem extends React.Component{

  constructor(props){
      super(props);
  }

  linkClick(searchStr, place_id) {
      const {user} = this.props.login;
      NProgress.start()

      var toSend = {
          searchStr: "",
          user_id: user._id,
          fulladdr: searchStr,
          button: false
      }
      this.props.searchBarRequest(toSend)
          .then(
              (res) => {
		  NProgress.done()
                  const token = res.data.token;
                  this.props.updateCoordsRequest(token);
                  const conf = {
                      showSearchResult: true,
                      placeFullAddr: token.placeFullAddr,
		      place_id: place_id,
                      placePhoto: "",
                      type: "jena"
                  }
                  if (token.descriptionArray) {
                      this.props.setDescriptionArray(token.descriptionArray);
                  } else {
                      this.props.setDescriptionArray({});
                  }
                  this.props.setShowSearchResult(conf);
                  browserHistory.push('/home');
              },
              (err) => {
                  var photo = "";
                  var obj = {
                    searchStr: searchStr
                  }
                  this.props.googlePlaceSearchRequest(obj).then(
                      (res) => {
		          NProgress.done()
                          const data = res.data.obj;
                          user.coords = {lat: data.lat, longt: data.lng};
                          user.directions = null

                          if (err.response.data.searchHistory) {
                              user.searchHistory = err.response.data.searchHistory;
                          }
                          if (err.response.data.autoDescription) {
                              user.autoDescription = err.response.data.autoDescription;
                          } else {
                              user.autoDescription = null
                          }

                          // Changing thw whole functionality. For now let descriptionArray is null
                          var descriptionArray = err.response.data.descriptionArray;
                          const conf = {
                              showSearchResult: true,
                              placeFullAddr:searchStr,
		              place_id: place_id,
                              placePhoto: data.photo,
                              type: "google"
                          }
                          this.props.setShowSearchResult(conf);

                          if (descriptionArray) {
                              this.props.setDescriptionArray(descriptionArray);
                          } else {
                              this.props.setDescriptionArray({});
                          }
                          this.props.updateCoordsRequest(user);
                          browserHistory.push('/home');
                      },
                      (err) => {
		          NProgress.done()
                          console.log("in googlePlaceSearch error client");
                      }
                  )
              }
          );
    }

    remove() {
        const {user} = this.props.login;
        var request = {
            location : this.props.location,
            user_id: user._id,
        }

        this.props.removeFavoritesAction(request).then(
            (res) => {
                console.log("Remove favorite successfully");
            },
            (err) => {
                console.log(err);
            });
    }

    render(){
        var {img} = this.props;

        if(img == '' || img == null){
            img = "http://www.mozmagic.com/files/assets/img/ui/no-image-available.png"
        } else if (img.indexOf("https") < 0) {
            img = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + img + "&key=" + config.googlePlaceApiKey
        }

        return(
            <div className="row">
		<div className="favourite-item-box" >
                    <img className="favourite-img col-sm-3 col-xs-12" src={img}
                         onClick={this.linkClick.bind(this, this.props.location, this.props.place_id)} />
                     <div className="col-sm-8 col-xs-11 favourite-content"
                         onClick={this.linkClick.bind(this, this.props.location, this.props.place_id)} >
                         {this.props.location}
                     </div>
                     <div className="btn col-sm-1 col-xs-1 glyphicon glyphicon-remove"
                          onClick={this.remove.bind(this)}/>
                </div>
            </div>
        );
    }
}

FavouriteItem.propTypes = {
    searchBarRequest: React.PropTypes.func.isRequired,
    login: React.PropTypes.object.isRequired,
    updateCoordsRequest: React.PropTypes.func.isRequired,
    setDescriptionArray: React.PropTypes.func.isRequired,
    setShowSearchResult: React.PropTypes.func.isRequired,
    googlePlaceSearchRequest: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, {searchBarRequest,
                                        updateCoordsRequest,
                                        setShowSearchResult,
                                        setDescriptionArray,
                                        removeFavoritesAction,
                                        googlePlaceSearchRequest})(FavouriteItem);
