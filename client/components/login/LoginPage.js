/*
 * LoginPage is the container component for LoginForm and WelcomeToSignup
 */

import React from 'react';
import LoginForm from './LoginForm';
import WelToSignup from './WelToSignup';
import OuterAuth from '../common/OuterAuth';
import { connect } from 'react-redux';
import { login } from '../../actions/authAction';
import { userLoginSocialRequest } from '../../actions/loginActionSocial'
import LinkToHome from './../common/LinkToHome';

class LoginPage extends React.Component {
    render() {
        const { login, userLoginSocialRequest } = this.props;
        return (
            <div className="container float_on_the_map-large white-background">
                <LinkToHome />
                <div className="row equal centered window-drop-shadow">
                    <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12 welcome-block">
                        <WelToSignup />
                    </div>
                    <div className="col-lg-5 col-md-6 col-sm-6 col-xs-6 login-block">
                        <LoginForm login={login} />
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 auth-block">
                        <OuterAuth userLoginSocialRequest={userLoginSocialRequest} />
                    </div>
                </div>
            </div>
        )
    }
}

LoginPage.propTypes = {
    login: React.PropTypes.func.isRequired,
    userLoginSocialRequest: React.PropTypes.func.isRequired
}

export default connect(null, { login , userLoginSocialRequest }) (LoginPage);
