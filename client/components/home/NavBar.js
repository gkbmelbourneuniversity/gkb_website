/*
 * This is component contains two part, Signup and Login button
 */

import React from 'react';
import { Link } from 'react-router';

class NavBar extends React.Component {

    constructor(props){
        super(props);
        this.state = {
        }
    }

    render(){
        const { isAuthenticated } = this.props.login;
        const guestNav =(
            <ul className="nav flex-column btn-on-map">
                <li className="nav-item">
                    <Link to="/signup" className="btn btn-nav">Signup</Link>
                </li>
                <li className="nav-item">
                    <Link to="/login" className="btn btn-nav">Login</Link>
                </li>
            </ul>
        );

        return(
            <div className ="no-click-through">{ !isAuthenticated && guestNav}</div>
        );
    }
}

NavBar.propTypes = {
    login: React.PropTypes.object.isRequired
}

export default NavBar;
