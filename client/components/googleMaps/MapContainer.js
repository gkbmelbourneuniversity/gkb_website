/*
 * This component is the top component, it contains the map, and all the other components are under this one.
 */

import React, {Component} from "react"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from "react-google-maps";
import { Router, Route, browserHistory } from 'react-router';
import LoginPage from '../login/LoginPage';
import SignupPage from '../signup/SignupPage';
import ResetPasswordPage from '../resetpwd/ResetPasswordPage';
import EmailSentPage from '../resetpwd/EmailSentPage';
import NewPwdPage from '../newpassword/NewPwdPage';
import WelcomePage from '../welcome/WelcomePage'
import HomePage from '../home/HomePage'
import {connect} from 'react-redux'
import AccountSetting from '../userProfile/AccountSettingPage';
import SearchHistory from '../userProfile/SearchHistoryPage';
import MyFavourites from '../userProfile/FavouritesPage';
import Landing from '../landing/LandingPage';
import MyContributions from '../userProfile/ContributionPage';
import Landmarker from './Landmarker';
import Place from './Place';
import config from '../../../server/config.js';

class MapContainer extends Component {

    constructor(props) {
      super(props);

      this.state = {
	currentPos: { lat: -37.8182711, lng: 144.9670618 },
        marker: {
          position: {
            lat: -37.8182711,
            lng: 144.9670618,
          }
        },
	showDirection: false,
        landmarks: [],
        showLandmarks: false,
      }

      this.requestDirection = this.requestDirection.bind(this);
      this.setSearchLocation = this.setSearchLocation.bind(this);
      this.setShowDirection = this.setShowDirection.bind(this);
      this.onDirectionsChanged = this.onDirectionsChanged.bind(this);
      this.fetchDirectionLandmarks = this.fetchDirectionLandmarks.bind(this);
    }

    componentWillMount() {
      var rest = require('rest');
      var map = this
      const {user} = this.props.login

      rest('http://api.ipstack.com/115.146.90.170?access_key=edc42ece0fcfa3836b3a3cdb3d89918b&output=json&legacy=1').then(function(response) {
          var parsedData = JSON.parse(response.entity)
          var lat = parsedData.latitude
          var lng = parsedData.longitude

          if (user != null && user.coords != null) {
              if (typeof(user.coords.lat) == 'string' ) {
                  lat = parseFloat(user.coords.lat)
                  lng = parseFloat(user.coords.longt)
              } else {
                  lat = (user.coords.lat)
                  lng = (user.coords.longt)
              }
          }
          map.state = {
		currentPos: {lat: lat, lng: lng},
		marker: {position: {lat:lat, lng:lng}},
	  }
      });
    }

    fetchDirectionLandmarks(directions) {
        var ret = []
	var requests = "["
        const steps = directions["routes"][0]["legs"][0]["steps"]

	for (var i = 0; i < steps.length; i++) {
	    var coordinates= JSON.stringify(steps[i]["end_location"])
            requests += coordinates.replace(/"/g, "").replace("{", "(").replace("}", ")") + ";"
	}
	requests = requests.substring(0, requests.length - 1) + "]"

	$.ajax({
            url: 'http://115.146.90.170:8080/intersectionLandmarks?coords=' + requests,
	    data: {
                format: 'json',
	    },
	    context: this,
	    type: 'GET',
	    success: function(data) {
	        this.setState({
	            directions: directions,
	            marker: {},
		    showDirection: true,
		    landmarks: data["landmarks"],
	        });
	    },
	    error: function(xhr, status, error) {
                console.log(xhr.responseText)
	    }
	})
    }

    requestDirection(dest) {
      const DirectionsService = new google.maps.DirectionsService();
      DirectionsService.route({
	origin: new google.maps.LatLng(this.state.currentPos),
	destination: new google.maps.LatLng(dest),
	travelMode: google.maps.TravelMode.WALKING,
      }, (result, status) => {
	if (status === google.maps.DirectionsStatus.OK) {
	    this.fetchDirectionLandmarks(result)
	} else {
	    console.error(`error fetching directions ${result}`);
	}
      });
    }

    setShowDirection(show) {
        this.setState({
	    showDirection: show,
	    marker: {position: this.state.currentPos},
	    directions: "",
            landmarks: [],
	})
    }

    setSearchLocation(lat, lon) {
	this.setState({
	    currentPos: { lat: parseFloat(lat), lng: parseFloat(lon) },
	    marker: {position: {lat:parseFloat(lat), lng:parseFloat(lon)}},
	    directions: "",
            landmarks: [],
	})
    }

    onDirectionsChanged() {
	this.fetchDirectionLandmarks(this.directionsRef.getDirections(), true)
    }

    render() {
      const url = "https://maps.googleapis.com/maps/api/js?key=" + config.googlePlaceApiKey + "&v=3.exp&libraries=places";
      const GettingStartedGoogleMap = withScriptjs(withGoogleMap(props => {
        return (
          <GoogleMap
            ref={props.onMapLoad}
            defaultZoom={15}
            defaultCenter={this.state.currentPos}
            onClick={props.onMapClick}
	    options={{
		    fullscreenControl: false,
		    mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	                position: google.maps.ControlPosition.RIGHT_BOTTOM
	            }
	    }}
          >
              <Router history={browserHistory}>
                  <Route path="/"
			component={Landing}
			setSearchLocation={this.setSearchLocation}
                  />
                  <Route path="home"
			component={HomePage}
			setSearchLocation={this.setSearchLocation}
		        directions = {this.state.directions}
		        landmarks = {this.state.landmarks}
		        showDirection = {this.state.showDirection}
		        requestDirection = {this.requestDirection}
		        setShowDirection = {this.setShowDirection}
		  />
                  <Route path="login" component={LoginPage}/>
                  <Route path="signup" component={SignupPage}/>
                  <Route path="resetpassword" component={ResetPasswordPage}/>
                  <Route path="emailsentpage" component={EmailSentPage}/>
                  <Route path="welcome" component={WelcomePage}/>
                  <Route path="newpwd" component={NewPwdPage}/>
                  <Route path="newpwd(/:email)" component={NewPwdPage}/>
                  <Route path="accountsetting" component={AccountSetting}/>
                  <Route path="searchhistory" component={SearchHistory}/>
                  <Route path="myfavourites" component={MyFavourites}/>
                  <Route path="mycontributions" component={MyContributions}/>
                  <Route path="place/:place_id" component={Place}
			setSearchLocation={this.setSearchLocation} />
              </Router>

              <Marker
                position={this.state.marker.position}
                onRightClick={() => props.onMarkerRightClick(this.state.marker)}
              />

            {this.state.landmarks && Object.entries(this.state.landmarks).map(function(landmarkSet, index){
                var landmark = landmarkSet[1]
                if (landmark) {
                return (
                <Landmarker
                        key={index}
                        position={{ lat: parseFloat(landmark.lattitude),
                                    lng: parseFloat(landmark.longitude)
                                 }}
                        name={landmark.name}
                />)}
            })}

	    { this.state.directions &&
	      <DirectionsRenderer options={{draggable: true}}
		                  ref={(r) => this.directionsRef = r}
		                  directions={this.state.directions}
		                  onDirectionsChanged={this.onDirectionsChanged}
	      />
	    }
          </GoogleMap>
        )
       }
      ))

      return (
        <div className="no_scroll" style={{height: '100%', width: '100%', position:'absolute'}}>
          <GettingStartedGoogleMap
              containerElement={
                <div style={{height: '100%', width: '100%'}} />
              }
              mapElement={
                <div style={{ height: "100%" }} />
              }
	      loadingElement={<div style={{ height: `100%` }} />}
	      googleMapURL={url}
          />
        </div>
      )
    }
}

MapContainer.propTypes = {
    login: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, {})(MapContainer);
